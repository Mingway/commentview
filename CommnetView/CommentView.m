//
//  CommentView.m
//  CommnetView
//
//  Created by shimingwei on 14-8-8.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#pragma mark -
#pragma mark CommentView

#import "CommentView.h"

@import QuartzCore;

static const float kMenuViewHeight = 40;
static const float kPadding = 5;

@interface CommentView ()<DrawingViewDelegate>{
    DrawingView *_drawingView;
}

@end

@implementation CommentView

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    for (UIView *subView in [self.subviews reverseObjectEnumerator]) {
        if (point.x > subView.frame.origin.x && point.x < subView.frame.origin.x + subView.frame.size.width && point.y > subView.frame.origin.y && point.y < subView.frame.origin.y + subView.frame.size.height && ![subView isKindOfClass:[DrawingView class]]) {
            CGPoint pointInSubview = [self convertPoint:point toView:subView];
            if ([subView isKindOfClass:[AnnotationPathView class]]) {
                AnnotationPathView *pathView = (AnnotationPathView*)subView;
                if (CGRectContainsPoint(pathView.frame, point)) {
                    return [subView hitTest:pointInSubview withEvent:event];
                }
            }else{
                return [subView hitTest:pointInSubview withEvent:event];
            }
        }
    }
    
    return _drawingView;
}

//Init
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _drawingView = [[DrawingView alloc] initWithFrame:frame penColor:[UIColor redColor] lineWidth:5.0f];
        _drawingView.delegate = self;
        [self addSubview:_drawingView];
        
        PathDataModel *pathDataModel = [PathDataModel defaultModel];
        NSArray *paths = [pathDataModel getPaths];
        NSArray *colors = [pathDataModel getColors];
        NSArray *lineWidths = [pathDataModel getLineWidths];
        NSArray *origins = [pathDataModel getOrigins];
        [self loadSubViewsWithPaths:paths colors:colors lineWidths:lineWidths origins:origins];
        
        AnnotationDataModel *annotationDataModel = [AnnotationDataModel defaultModel];
        NSArray *rects = [annotationDataModel getRects];
        NSArray *texts = [annotationDataModel getTexts];
        [self loadSubViewsWithRects:rects texts:texts];
    }
    return self;
}

- (void)loadSubViewsWithPaths:(NSArray*)paths colors:(NSArray*)colors lineWidths:(NSArray*)lineWidths origins:(NSArray*)origins
{
    for (int i = 0; i < paths.count && i < colors.count && i < lineWidths.count; i ++) {
        CGPathRef path = [paths[i] CGPath];
        UIColor *color = colors[i];
        CGFloat lineWidth = [lineWidths[i] floatValue];
        CGPoint point = [origins[i] CGPointValue];
        
        AnnotationPathView *pathView = (AnnotationPathView*)[self addAnnotationPathViewWithColor:color lineWidth:lineWidth path:path];
        [pathView setFrame:CGRectMake(point.x, point.y, CGRectGetWidth(pathView.frame) , CGRectGetHeight(pathView.frame))];
    }
}

- (void)loadSubViewsWithRects:(NSArray*)rects texts:(NSArray*)texts
{
    for (int i = 0; i < rects.count && i < texts.count; i ++) {
        AnnotationView *annotationView = [[AnnotationView alloc] initWithFrame:[rects[i] CGRectValue]];
        [annotationView setText:texts[i]];
        [self addSubview:annotationView];
    }
}

- (void)saveCurrentAnnotations
{
    [_drawingView addSubviewFromCurrentPath];
    NSMutableArray *paths = [NSMutableArray array];
    NSMutableArray *colors = [NSMutableArray array];
    NSMutableArray *lineWidths = [NSMutableArray array];
    NSMutableArray *origins = [NSMutableArray array];
    
    NSMutableArray *rects = [NSMutableArray array];
    NSMutableArray *texts = [NSMutableArray array];
    
    for (id subView in self.subviews) {
        if ([subView isKindOfClass:[AnnotationPathView class]]) {
            [paths addObject:[UIBezierPath bezierPathWithCGPath:[subView getPath]]];
            [colors addObject:[subView getPenColor]];
            [lineWidths addObject:@([subView getLineWidth])];
            [origins addObject:[NSValue valueWithCGPoint:CGPointMake([subView frame].origin.x, [subView frame].origin.y)]];
        }else if ([subView isKindOfClass:[AnnotationView class]]){
            [rects addObject:[NSValue valueWithCGRect:[subView frame]]];
            [texts addObject:[subView getText]];
        }
    }
    //保存绘画路径
    [[PathDataModel defaultModel] savePaths:paths colors:colors lineWidths:lineWidths origins:origins];
    
    //保存批注文本信息
    [[AnnotationDataModel defaultModel] saveRects:rects texts:texts];
}

//DrawingViewDelegate
- (UIView*)addAnnotationPathViewWithColor:(UIColor *)color lineWidth:(CGFloat)lineWidth path:(CGPathRef)path
{
    CGRect frame = CGPathGetBoundingBox(path);
    frame.origin.x -=3;
    frame.origin.y -= 3;
    frame.size.width += 6;
    frame.size.height += 6;
    
    if (frame.size.width < 100) {
        frame.size.width = 100;
    }
    
    if (frame.origin.y - (kMenuViewHeight + kPadding) >=0 ){
        frame.origin.y -= (kMenuViewHeight + kPadding);
        frame.size.height += (kMenuViewHeight + kPadding);
    }
    
    CGAffineTransform transform =  CGAffineTransformMakeTranslation(-frame.origin.x,-frame.origin.y);
    
    CGPathRef newPath = CGPathCreateCopyByTransformingPath(path,&transform);
    
    AnnotationPathView* pathView = [[AnnotationPathView alloc]initWithFrame:frame penColor:color lineWidth:lineWidth];
    [pathView setPath:newPath];
    [self insertSubview:pathView belowSubview:_drawingView];
    return pathView;
}

- (void)setPenColor:(UIColor *)penColor
{
    [_drawingView addSubviewFromCurrentPath];
    [_drawingView setPenColor:penColor];
}

- (void)setLineWidth:(CGFloat)lineWidth
{
    [_drawingView addSubviewFromCurrentPath];
    [_drawingView setLineWidth:lineWidth];
}

- (void)clear
{
    [_drawingView clear];
    for (id view in self.subviews) {
        if ([view isKindOfClass:[UIView class]] && ![view isKindOfClass:[DrawingView class]]) {
            [view removeFromSuperview];
        }
    }
}

- (void)addAnnotationView
{
    AnnotationView *annotationView = [[AnnotationView alloc] initWithFrame:CGRectMake(0, 0, 200, 250)];
    annotationView.center = self.center;
    [self addSubview:annotationView];
}

@end


#pragma mark -
#pragma mark DrawingView

@interface DrawingView (){
    UIBezierPath *_path;
    UIImage *_incrementalImage;
    CGPoint _pts[5];
    uint _ctr;
    UIColor *_penColor;
    CGFloat _lineWidth;
}

@end

@implementation DrawingView

//Init
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame penColor:(UIColor *)penColor lineWidth:(CGFloat)lineWidth{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMultipleTouchEnabled:NO];
        self.backgroundColor = [UIColor clearColor];
        _penColor = penColor;
        _lineWidth = lineWidth;
        _path = [UIBezierPath bezierPath];
    }
    return self;
}

- (void)setPenColor:(UIColor *)penColor
{
    _penColor = penColor;
}

- (void)setLineWidth:(CGFloat)lineWidth
{
    _lineWidth = lineWidth;
}

//Dealloc
-(void)dealloc
{
    
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (_path){
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        [_penColor set];
        CGContextSetLineWidth(ctx,_lineWidth);
        CGContextAddPath(ctx,_path.CGPath);
        CGContextStrokePath(ctx);
    }
}

//Touch Method
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(addSubviewFromCurrentPath) object:nil];
    
    UITouch* touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    
    _ctr = 0;
    _pts[0] = touchPoint;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    
    _ctr++;
    _pts[_ctr] = touchPoint;
    if (_ctr == 4)
    {
        _pts[3] = CGPointMake((_pts[2].x + _pts[4].x)/2.0, (_pts[2].y + _pts[4].y)/2.0);
        
        [_path moveToPoint:_pts[0]];
        [_path addCurveToPoint:_pts[3] controlPoint1:_pts[1] controlPoint2:_pts[2]];
        
        [self setNeedsDisplay];
        _pts[0] = _pts[3];
        _pts[1] = _pts[4];
        _ctr = 1;
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    _ctr = 0;
    [self performSelector:@selector(addSubviewFromCurrentPath) withObject:nil afterDelay:1.5];
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)addSubviewFromCurrentPath
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(addSubviewFromCurrentPath) object:nil];
    if (_delegate && [_delegate respondsToSelector:@selector(addAnnotationPathViewWithColor:lineWidth:path:)]) {
        [_delegate addAnnotationPathViewWithColor:_penColor lineWidth:_lineWidth path:_path.CGPath];
    }
    [self clear];
}

- (void)clear
{
    [_path removeAllPoints];
    _ctr = 0;
    [self setNeedsDisplay];
}

@end

#pragma mark -
#pragma mark AnnotationPathView

@interface AnnotationPathView ()<MenuViewDelegate>{
    CGPathRef _currentPath;
    UIColor *_penColor;
    CGFloat _lineWidth;
    MenuView *_menuView;
    
    BOOL _isEditMode;
    UIPanGestureRecognizer *_panGesture;
    UITapGestureRecognizer *_tapGesture;
}

@end

@implementation AnnotationPathView

//Init
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(_menuView.frame, point)) {
        CGPoint pointInSubView = [self convertPoint:point toView:_menuView];
        return [_menuView hitTest:pointInSubView withEvent:event];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame penColor:(UIColor *)penColor lineWidth:(CGFloat)lineWidth{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _penColor = penColor;
        _lineWidth = lineWidth;
        
        _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        [self addGestureRecognizer:_panGesture];
        
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:_tapGesture];
        
        _menuView = [[MenuView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), kMenuViewHeight)];
        _menuView.delegate = self;
        [self addSubview:_menuView];
        
        [self setUneditMode];
    }
    return self;
}

- (void)setEditMode
{
    [_menuView setFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), kMenuViewHeight)];
    _menuView.hidden = NO;
}

- (void)setUneditMode
{
    _menuView.hidden = YES;
}

- (void)handlePanGesture:(UIPanGestureRecognizer*)pan
{
    if (_isEditMode) {
        CGPoint translation = [pan translationInView:pan.view];
        pan.view.center=CGPointMake(pan.view.center.x+translation.x, pan.view.center.y+ translation.y);
        [pan setTranslation:CGPointMake(0, 0) inView:pan.view];
        
        if (pan.state == UIGestureRecognizerStateBegan) {
            self.alpha = 0.5;
        }else if (pan.state == UIGestureRecognizerStateChanged){
        }else if (pan.state == UIGestureRecognizerStateEnded ||
                  pan.state == UIGestureRecognizerStateCancelled ||
                  pan.state == UIGestureRecognizerStateFailed){
            self.alpha = 1.0;
        }
    }
}

- (void)handleTapGesture:(UITapGestureRecognizer*)tap
{
    _isEditMode = !_isEditMode;
    if (_isEditMode) {
        [self setEditMode];
    }else{
        [self setUneditMode];
    }
    [self setNeedsDisplay];
}

- (CGPathRef)getPath
{
    return _currentPath;
}

- (UIColor*)getPenColor
{
    return _penColor;
}

- (CGFloat)getLineWidth
{
    return _lineWidth;
}

- (BOOL)isInEditMode
{
    return _isEditMode;
}

//Dealloc
-(void)dealloc
{
    [self removeGestureRecognizer:_tapGesture];
    [self removeGestureRecognizer:_panGesture];
    CGPathRelease(_currentPath);
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (_currentPath){
        [_penColor set];
        CGContextSetLineWidth(context,_lineWidth);
        CGContextAddPath(context,_currentPath);
        CGContextStrokePath(context);
    }
    
    
    if (_isEditMode) {
        CGRect subRect = CGRectMake(0, kMenuViewHeight + kPadding, CGRectGetWidth(rect), CGRectGetHeight(rect) - kMenuViewHeight - kPadding);
        
        CGFloat lengths[] = {5,5,5,5};
        CGContextSetRGBStrokeColor(context, 0.5, 0.5, 0.5, 1.0);
        CGContextSetLineDash(context, 0, lengths, 4);
        CGContextSetLineWidth(context, 1.0);
        CGContextAddRect(context, subRect);
        CGContextDrawPath(context, kCGPathStroke);
        
        CGContextSetRGBFillColor(context, 0.7, 0.7, 0.7, 0.3);
        CGContextAddRect(context, subRect);
        CGContextDrawPath(context, kCGPathFill);
    }
}

- (void)setPath:(CGPathRef)newPath
{
    CGPathRelease(_currentPath);
    _currentPath = CGPathRetain(newPath);
    [self setNeedsDisplay];
}

//Touch Method
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_isEditMode) {
        [self.nextResponder touchesBegan:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_isEditMode) {
        [self.nextResponder touchesMoved:touches withEvent:event];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_isEditMode) {
        [self.nextResponder touchesEnded:touches withEvent:event];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_isEditMode) {
        [self.nextResponder touchesCancelled:touches withEvent:event];
    }
}

#pragma mark -
#pragma mark MenuViewDelegate

- (void)removeView
{
    [self removeFromSuperview];
}

@end

#pragma mark -
#pragma mark PathDataModel

@interface PathDataModel (){
    NSArray *_paths;
    NSArray *_colors;
    NSArray *_lineWidths;
    NSArray *_origins;
}

@end

@implementation PathDataModel

+ (instancetype)defaultModel
{
    static PathDataModel *model = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[PathDataModel alloc] init];
    });
    return model;
}

- (void)savePaths:(NSArray*)paths colors:(NSArray*)colors lineWidths:(NSArray*)lineWidths origins:(NSArray*)origins
{
    _paths = paths;
    _colors = colors;
    _lineWidths = lineWidths;
    _origins = origins;
}

- (NSArray*)getPaths
{
    return _paths;
}

- (NSArray*)getColors
{
    return _colors;
}

- (NSArray*)getLineWidths
{
    return _lineWidths;
}

- (NSArray*)getOrigins
{
    return _origins;
}

@end


#pragma mark -
#pragma mark AnnotationDataModel

@interface AnnotationDataModel (){
    NSArray *_rects;
    NSArray *_texts;
}

@end

@implementation AnnotationDataModel

+ (instancetype)defaultModel
{
    static AnnotationDataModel *model = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[AnnotationDataModel alloc] init];
    });
    return model;
}

- (void)saveRects:(NSArray*)rects texts:(NSArray*)texts
{
    _rects = rects;
    _texts = texts;
}

- (NSArray*)getRects
{
    return _rects;
}

- (NSArray*)getTexts
{
    return _texts;
}

@end

#pragma mark -
#pragma mark AnnotationView

static const float kTextViewPadding = 3;
static const float kMovePointPadding = 40;

@interface AnnotationView ()<MenuViewDelegate>{
    AnnotationTextView *_inputView;
    CGPoint _originalPoint;
    MenuView *_menuView;
    
    NSInteger _index;
    UILongPressGestureRecognizer *_longPressGesture;
    BOOL _isEditMode;
    UITapGestureRecognizer *_tapGesture;
    
    UIButton *_leftTopBtn;
    UIButton *_leftBottomBtn;
    UIButton *_rightTopBtn;
    UIButton *_rightBottonBtn;
    UIPanGestureRecognizer *_panGesture;
}

@end

@implementation AnnotationView

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(_menuView.frame, point)) {
        CGPoint pointInSubView = [self convertPoint:point toView:_menuView];
        return [_menuView hitTest:pointInSubView withEvent:event];
    }
    if (CGRectContainsPoint(_leftTopBtn.frame, point)) {
        return _leftTopBtn;
    }
    if (CGRectContainsPoint(_leftBottomBtn.frame, point)) {
        return _leftBottomBtn;
    }
    if (CGRectContainsPoint(_rightTopBtn.frame, point)) {
        return _rightTopBtn;
    }
    if (CGRectContainsPoint(_rightBottonBtn.frame, point)) {
        return _rightBottonBtn;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _inputView = [[AnnotationTextView alloc] init];
        [self setInputViewFrameWithFrame:frame];
        [self addSubview:_inputView];
        
        _menuView = [[MenuView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), kMenuViewHeight)];
        _menuView.delegate = self;
        [self addSubview:_menuView];
        
        _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
        [self addGestureRecognizer:_longPressGesture];
        
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:_tapGesture];
        
        _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        
        _leftTopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_leftTopBtn];
        [_leftTopBtn setBackgroundImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
        
        
        _leftBottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_leftBottomBtn];
        [_leftBottomBtn setBackgroundImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
        
        
        _rightTopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_rightTopBtn];
        [_rightTopBtn setBackgroundImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
        
        
        _rightBottonBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_rightBottonBtn];
        [_rightBottonBtn setBackgroundImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
        
        
        [self addGestureRecognizer:_panGesture];
        
        [self setUneditMode];
        [self setMovePointBtnFrameWithFrame:frame];
        _index = -1;
    }
    return self;
}

- (void)setEditMode
{
    _menuView.hidden = NO;
    _leftTopBtn.hidden = NO;
    _leftBottomBtn.hidden = NO;
    _rightTopBtn.hidden = NO;
    _rightBottonBtn.hidden = NO;
}

- (void)setUneditMode
{
    _menuView.hidden = YES;
    _leftTopBtn.hidden = YES;
    _leftBottomBtn.hidden = YES;
    _rightTopBtn.hidden = YES;
    _rightBottonBtn.hidden = YES;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (_isEditMode) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGRect subRect = CGRectMake( kMovePointPadding/2, kMenuViewHeight + kPadding + kMovePointPadding/2, CGRectGetWidth(rect) - kMovePointPadding, CGRectGetHeight(rect) - kMenuViewHeight - kPadding - kMovePointPadding);
        
        CGFloat lengths[] = {5,5,5,5};
        CGContextSetRGBStrokeColor(context, 0.5, 0.5, 0.5, 1.0);
        CGContextSetLineDash(context, 0, lengths, 4);
        CGContextSetLineWidth(context, 1.0);
        CGContextAddRect(context, subRect);
        CGContextDrawPath(context, kCGPathStroke);
        
        CGContextSetRGBFillColor(context, 0.7, 0.7, 0.7, 0.3);
        CGContextAddRect(context, subRect);
        CGContextDrawPath(context, kCGPathFill);
    }
}

- (void)dealloc
{
    [self removeGestureRecognizer:_panGesture];
    [self removeGestureRecognizer:_tapGesture];
    [self removeGestureRecognizer:_longPressGesture];
}

- (NSString*)getText
{
    return _inputView.text;
}

- (void)setText:(NSString*)text
{
    [_inputView setText:text];
}

- (void)setInputViewFrameWithFrame:(CGRect)frame
{
    [_inputView setFrame:CGRectMake(kTextViewPadding + kMovePointPadding, kMenuViewHeight + kPadding + kTextViewPadding + kMovePointPadding, CGRectGetWidth(frame) - 2 * kTextViewPadding - 2 * kMovePointPadding, CGRectGetHeight(frame) - kPadding - kMenuViewHeight - 2 * kTextViewPadding - 2 * kMovePointPadding)];
}

- (void)setMovePointBtnFrameWithFrame:(CGRect)frame
{
    [_leftTopBtn setFrame:CGRectMake(0, kMenuViewHeight + kPadding, kMovePointPadding, kMovePointPadding)];
    [_leftBottomBtn setFrame:CGRectMake(0, CGRectGetHeight(frame) - kMovePointPadding, kMovePointPadding, kMovePointPadding)];
    [_rightTopBtn setFrame:CGRectMake(CGRectGetWidth(frame) - kMovePointPadding, kMenuViewHeight + kPadding, kMovePointPadding, kMovePointPadding)];
    [_rightBottonBtn setFrame:CGRectMake(CGRectGetWidth(frame) - kMovePointPadding, CGRectGetHeight(frame) - kMovePointPadding, kMovePointPadding, kMovePointPadding)];
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer*)longPress
{
    [_inputView becomeFirstResponder];
}

- (void)handleTapGesture:(UITapGestureRecognizer*)tap
{
    _isEditMode = !_isEditMode;
    
    if (_isEditMode) {
        [self setEditMode];
    }else{
        [self setUneditMode];
    }
    [self setNeedsDisplay];
}

- (void)handlePanGesture:(UIPanGestureRecognizer*)pan
{
    if (_isEditMode) {
        if (pan.state == UIGestureRecognizerStateBegan) {
            self.alpha = 0.5;
            if (CGRectContainsPoint(CGRectMake(CGRectGetMidX(_leftTopBtn.frame) - 10, CGRectGetMinY(_leftTopBtn.frame) - 10, CGRectGetWidth(_leftTopBtn.frame) + 40, CGRectGetHeight(_leftTopBtn.frame) + 40), [pan locationInView:pan.view])) {
                _index = 1;
            }else if (CGRectContainsPoint(CGRectMake(CGRectGetMidX(_leftBottomBtn.frame) - 10, CGRectGetMinY(_leftBottomBtn.frame) - 30, CGRectGetWidth(_leftBottomBtn.frame) + 40, CGRectGetHeight(_leftBottomBtn.frame) + 20), [pan locationInView:pan.view])){
                _index = 2;
            }else if (CGRectContainsPoint(CGRectMake(CGRectGetMidX(_rightTopBtn.frame) - 30, CGRectGetMinY(_rightTopBtn.frame) - 10, CGRectGetWidth(_rightTopBtn.frame) + 20, CGRectGetHeight(_rightTopBtn.frame) + 40), [pan locationInView:pan.view])){
                _index = 3;
            }else if (CGRectContainsPoint(CGRectMake(CGRectGetMidX(_rightBottonBtn.frame) - 30, CGRectGetMinY(_rightBottonBtn.frame) - 30, CGRectGetWidth(_rightBottonBtn.frame) + 20, CGRectGetHeight(_rightBottonBtn.frame) + 20), [pan locationInView:pan.view])){
                _index = 4;
            }else{
                _index = 5;
            }
        }else if (pan.state == UIGestureRecognizerStateChanged) {
            CGPoint translation = [pan translationInView:self];
            
            switch (_index) {
                case 1:{
                    CGRect newRect = CGRectMake(CGRectGetMinX(self.frame) + translation.x,
                                                CGRectGetMinY(self.frame) + translation.y,
                                                CGRectGetWidth(self.frame) - translation.x,
                                                CGRectGetHeight(self.frame) - translation.y);
                    if (CGRectGetWidth(newRect) < 150 || CGRectGetHeight(newRect) < 200) {
                        return;
                    }
                    self.frame = newRect;
                    [self setInputViewFrameWithFrame:self.frame];
                    [self setMovePointBtnFrameWithFrame:self.frame];
                    [self setNeedsDisplay];
                }
                    break;
                case 2:{
                    CGRect newRect = CGRectMake(CGRectGetMinX(self.frame) + translation.x,
                                                CGRectGetMinY(self.frame),
                                                CGRectGetWidth(self.frame) - translation.x,
                                                CGRectGetHeight(self.frame) + translation.y);
                    if (CGRectGetWidth(newRect) < 150 || CGRectGetHeight(newRect) < 200) {
                        return;
                    }
                    self.frame = newRect;
                    [self setInputViewFrameWithFrame:self.frame];
                    [self setMovePointBtnFrameWithFrame:self.frame];
                    [self setNeedsDisplay];
                }
                    break;
                case 3:{
                    CGRect newRect = CGRectMake(CGRectGetMinX(self.frame),
                                                CGRectGetMinY(self.frame) + translation.y,
                                                CGRectGetWidth(self.frame) + translation.x,
                                                CGRectGetHeight(self.frame) - translation.y);
                    if (CGRectGetWidth(newRect) < 150 || CGRectGetHeight(newRect) < 200) {
                        return;
                    }
                    self.frame = newRect;
                    [self setInputViewFrameWithFrame:self.frame];
                    [self setMovePointBtnFrameWithFrame:self.frame];
                    [self setNeedsDisplay];
                }
                    break;
                case 4:{
                    CGRect newRect = CGRectMake(CGRectGetMinX(self.frame),
                                                CGRectGetMinY(self.frame),
                                                CGRectGetWidth(self.frame) + translation.x,
                                                CGRectGetHeight(self.frame) + translation.y);
                    if (CGRectGetWidth(newRect) < 150 || CGRectGetHeight(newRect) < 200) {
                        return;
                    }
                    self.frame = newRect;
                    [self setInputViewFrameWithFrame:self.frame];
                    [self setMovePointBtnFrameWithFrame:self.frame];
                    [self setNeedsDisplay];
                }
                    break;
                case 5:{
                    pan.view.center=CGPointMake(pan.view.center.x+translation.x, pan.view.center.y+ translation.y);
                }
                    break;
                default:
                    break;
            }
            [pan setTranslation:CGPointMake(0, 0) inView:pan.view];
        }else if (pan.state == UIGestureRecognizerStateEnded ||
            pan.state == UIGestureRecognizerStateFailed ||
            pan.state == UIGestureRecognizerStateCancelled) {
            self.alpha = 1.0;
        }
    }
}
 
#pragma mark -
#pragma mark MenuViewDelegate

- (void)removeView
{
    [self removeFromSuperview];
}

@end


#pragma mark -
#pragma mark AnnotationTextView

#define PLACEHODLER         @"长按输入文本......"

@interface AnnotationTextView ()<UITextViewDelegate>

@end

@implementation AnnotationTextView

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.layer.borderColor = [UIColor grayColor].CGColor;
        self.layer.borderWidth = 1;
        self.backgroundColor = [UIColor colorWithRed:1.0 green:0.5 blue:0.0 alpha:0.7];
        self.font = [UIFont systemFontOfSize:17.0f];
        self.text = PLACEHODLER;
        self.textColor = [UIColor grayColor];
    }
    return self;
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    if (![text isEqualToString:PLACEHODLER]) {
        self.textColor = [UIColor blackColor];
    }
}

//Touch Method
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.nextResponder touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    [self.nextResponder touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.nextResponder touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    [self.nextResponder touchesCancelled:touches withEvent:event];
}

#pragma mark -
#pragma mark UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:PLACEHODLER]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = PLACEHODLER;
        textView.textColor = [UIColor grayColor]; //optional
    }
    [textView resignFirstResponder];
}

@end

#pragma mark -
#pragma mark MenuView

@interface MenuView (){
    UIButton *_deleteBtn;
}

@end

@implementation MenuView

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(_deleteBtn.frame, point)) {
        return _deleteBtn;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setFrame:CGRectMake(0, 0, 40, CGRectGetHeight(frame))];
        [_deleteBtn addTarget:self action:@selector(removeView) forControlEvents:UIControlEventTouchUpInside];
        [_deleteBtn setBackgroundColor:[UIColor clearColor]];
        [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"remove"] forState:UIControlStateNormal];
        [self addSubview:_deleteBtn];
    }
    return self;
}

- (void)removeView
{
    if (_delegate && [_delegate respondsToSelector:@selector(removeView)]) {
        [_delegate removeView];
    }
}

@end
