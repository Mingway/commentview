//
//  ViewController.m
//  CommnetView
//
//  Created by shimingwei on 14-8-8.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ViewController.h"
#import "CommentView.h"

@interface ViewController (){
    CommentView *_commentView;
}

- (IBAction)addAnnotation:(UIButton *)sender;
- (IBAction)changeLineWidth:(UISlider *)sender;
- (IBAction)changeColor:(UISegmentedControl *)sender;
- (IBAction)clear:(UIButton *)sender;
- (IBAction)save:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _commentView = [[CommentView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    [self.view insertSubview:_commentView atIndex:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)addAnnotation:(UIButton *)sender {
    [_commentView addAnnotationView];
}

- (IBAction)changeLineWidth:(UISlider *)sender {
    [_commentView setLineWidth:sender.value];
}

- (IBAction)changeColor:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [_commentView setPenColor:[UIColor redColor]];
            break;
        case 1:
            [_commentView setPenColor:[UIColor greenColor]];
            break;
        case 2:
            [_commentView setPenColor:[UIColor blueColor]];
            break;
        case 3:
            [_commentView setPenColor:[UIColor blackColor]];
            break;
        default:
            [_commentView setPenColor:[UIColor redColor]];
            break;
    }
}

- (IBAction)clear:(UIButton *)sender {
    [_commentView clear];
}

- (IBAction)save:(UIButton *)sender {
    [_commentView saveCurrentAnnotations];
}

@end
