//
//  CommentView.h
//  CommnetView
//
//  Created by shimingwei on 14-8-8.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

/*  -------------------------------------------------------------------------------------------------------
 
 画图View代理方法
 
 */

@protocol DrawingViewDelegate <NSObject>

- (UIView*)addAnnotationPathViewWithColor:(UIColor*)color lineWidth:(CGFloat)lineWidth path:(CGPathRef)path;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 批注菜单代理方法
 
 */

@protocol MenuViewDelegate <NSObject>

- (void)removeView;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 包含两种subview，分别是drawingView和annoatationView。drawingView用于画图形；annotationView用于画出drawingView所画出的图形，使每一部分成为一个个体。
 
 */

@interface CommentView : UIView

- (void)saveCurrentAnnotations;

- (void)setPenColor:(UIColor *)penColor;
- (void)setLineWidth:(CGFloat)lineWidth;
- (void)clear;

- (void)addAnnotationView;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 touch事件画图形，保存路径，用于AnnotationPathView画出图形。保存路径后clear所有界面内容，以待下次继续画图。
 
 */

@interface DrawingView : UIView

@property (nonatomic)id<DrawingViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame penColor:(UIColor *)penColor lineWidth:(CGFloat)lineWidth;

- (void)setPenColor:(UIColor *)penColor;
- (void)setLineWidth:(CGFloat)lineWidth;

- (void)clear;

- (void)addSubviewFromCurrentPath;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 根据Path画出图形
 
 */

@interface AnnotationPathView : UIView

- (instancetype)initWithFrame:(CGRect)frame penColor:(UIColor *)penColor lineWidth:(CGFloat)lineWidth;

-(void)setPath:(CGPathRef)newPath;

- (CGPathRef)getPath;
- (UIColor*)getPenColor;
- (CGFloat)getLineWidth;

- (BOOL)isInEditMode;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 保存之前的绘画路径
 
 */

@interface PathDataModel : NSObject

+ (instancetype)defaultModel;

- (void)savePaths:(NSArray*)paths colors:(NSArray*)colors lineWidths:(NSArray*)lineWidths origins:(NSArray*)origins;
- (NSArray*)getPaths;
- (NSArray*)getColors;
- (NSArray*)getLineWidths;
- (NSArray*)getOrigins;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 保存之前的批注文本信息
 
 */
@interface AnnotationDataModel : NSObject

+ (instancetype)defaultModel;

- (void)saveRects:(NSArray*)rects texts:(NSArray*)texts;
- (NSArray*)getRects;
- (NSArray*)getTexts;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 输入文本信息的InputView
 
 */

@interface AnnotationView : UIView

- (void)setText:(NSString*)text;
- (NSString*)getText;

@end

/*  -------------------------------------------------------------------------------------------------------
 
 文本TextView
 
 */

@interface AnnotationTextView : UITextView

@end

/*  -------------------------------------------------------------------------------------------------------
 
 每个批注View上面的控制菜单
 
 */

@interface MenuView : UIView

@property (nonatomic)id<MenuViewDelegate> delegate;
@property (nonatomic, strong) UIButton *deleteBtn;

@end

